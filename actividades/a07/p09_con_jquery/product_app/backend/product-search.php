<?php
    use API\Productos;
    include_once __DIR__.'/API/Productos.php';

    $productos = new Productos();
    $productos->search($_GET['search']);
    echo $productos->getResponse();
?>