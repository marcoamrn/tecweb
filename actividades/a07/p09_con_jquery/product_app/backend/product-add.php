<?php
    use API\Productos;
    include_once __DIR__.'/API/Productos.php';

    $producto = json_decode(json_encode($_POST));

    $productos = new Productos();
    $productos->add($producto);
    echo $productos->getResponse();
?>