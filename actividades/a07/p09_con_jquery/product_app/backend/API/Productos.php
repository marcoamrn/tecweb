<?php
    namespace API;
    require_once __DIR__ . '/DataBase.php';
    class Productos extends DataBase{
        private $response = [];

        public function __construct($response = null){
            parent::__construct('marketzone');
            $this->response = $response;
        }

        public function add($producto){
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );
            if(!empty($producto)) {
                $sql = "SELECT * FROM productos WHERE nombre = '{$producto->nombre}' AND eliminado = 0";
                $result = $this->conexion->query($sql);
                
                if ($result->num_rows == 0) {
                    $this->conexion->set_charset("utf8");
                    $sql = "INSERT INTO productos VALUES (null, '{$producto->nombre}', '{$producto->marca}', '{$producto->modelo}', {$producto->precio}, '{$producto->detalles}', {$producto->unidades}, '{$producto->imagen}', 0)";
                    if($this->conexion->query($sql)){
                        $this->response['status'] =  "success";
                        $this->response['message'] =  "Producto agregado";
                    } else {
                        $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                    }
                }
                $result->free();
                $this->conexion->close();
            }
        }

        public function delete($id){
            $this->response = array(
                'status'  => 'error',
                'message' => 'No se pudo eliminar'
            );
            if( isset($id) ) {
                $sql = "UPDATE productos SET eliminado=1 WHERE id = {$id}";
                if ( $this->conexion->query($sql) ) {
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto eliminado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
                $this->conexion->close();
            }
        }

        public function edit($producto){
            $query = "UPDATE productos SET nombre = '{$producto->nombre}', marca = '{$producto->marca}', modelo = '{$producto->modelo}', precio = '{$producto->precio}', detalles = '{$producto->detalles}', unidades = '{$producto->unidades}', imagen = '{$producto->imagen}', eliminado = 0 WHERE id = '{$producto->id}'";
            if( $this->conexion->query($query) ) {
                $this->response['status'] =  "success";
                $this->response['message'] =  "Producto actualizado";
            } else {
                $this->response['message'] = "ERROR: No se ejecuto $query. " . mysqli_error($this->conexion);
            }
            $this->conexion->close();
        }

        public function list(){
            if ( $result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0") ) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
        
                if(!is_null($rows)) {
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
            $this->conexion->close();
        }

        public function search($search){
            if( isset($search) ) {
                $sql = "SELECT * FROM productos WHERE (id = '{$search}' OR nombre LIKE '%{$search}%' OR marca LIKE '%{$search}%' OR detalles LIKE '%{$search}%') AND eliminado = 0";
                if ( $result = $this->conexion->query($sql) ) {
                    $rows = $result->fetch_all(MYSQLI_ASSOC);

                    if(!is_null($rows)) {
                        foreach($rows as $num => $row) {
                            foreach($row as $key => $value) {
                                $this->response[$num][$key] = utf8_encode($value);
                            }
                        }
                    }
                    $result->free();
                } else {
                    die('Query Error: '.mysqli_error($this->conexion));
                }
                $this->conexion->close();
            } 
        }

        public function single($id){
            $query = "SELECT * FROM productos WHERE id = $id";

            $result = mysqli_query($this->conexion, $query);
            if(!$result){
                die('Query Failed');
            }
            while($row = mysqli_fetch_array($result)) {
                $this->response = array(
                    'nombre' => $row['nombre'],
                    'marca' => $row['marca'],
                    'modelo' => $row['modelo'],
                    'precio' => $row['precio'],
                    'detalles' => $row['detalles'],
                    'unidades' => $row['unidades'],
                    'imagen' => $row['imagen'],
                    'id' => $row['id']
                );
            }
            $result->free();
            $this->conexion->close();
        }

        public function singleByName($nombre){
            if( isset($nombre) ) {
                if ( $result = $this->conexion->query("SELECT * FROM productos WHERE nombre = '{$nombre}'") ) {
                    $row = $result->fetch_array(MYSQLI_ASSOC);
        
                    if(!is_null($row)) {
                        foreach($row as $key => $value) {
                            $this->response[$key] = utf8_encode($value);
                        }
                    }
                    $result->free();
                } else {
                    die('Query Error: '.mysqli_error($this->conexion));
                }
                $this->conexion->close();
            }
        }

        public function getResponse(){
            return json_encode($this->response, JSON_PRETTY_PRINT);
        }
    }
?>