$(document).ready(function () {
    let edit = false;
    $('#estado').hide();
    $('#product-result').hide();
    fetchProducts();

    $('#search').keyup(function (e) {
        if ($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                type: 'GET',
                data: { search },
                success: function (response) {
                    console.log(response);
                    let products = JSON.parse(response);
                    let template = '';

                    products.forEach(product => {
                        template += `<li>
                            ${product.nombre}
                        </li>`
                    });

                    $('#container').html(template);
                    $('#product-result').show();
                }
            });
        }
    });

    var nombre;
    var precio;
    var unidades;
    var modelo;
    var marca;
    var detalles;
    var imagen;
    var plantilla;

    $('#name').keyup(function (e) {
        if ($('#name').val()) {
            nombre = $('#name').val();
            $.ajax({
                url: 'backend/product-exist.php',
                type: 'GET',
                data: { nombre },
                success: function (response) {
                    let resultado = JSON.parse(response);

                    if(resultado.length == 0){
                        plantilla = `<li><p>Nombre disponible</p></li>`
                    }
                    else{
                        plantilla = `<li><p>Ya existe un producto con este nombre</p></li>`
                    }

                    $('#contenedor').html(plantilla);
                    $('#estado').show();
                }
            });
        }
    });

    $( "#price" ).blur(function () {
        precio = parseFloat($('#price').val());
        if(precio == null || precio == ''){
            alert("Ingresar el precio");
            plantilla = '<li><p>Ingresar el precio</p></li>'
            $('#contenedor').html(plantilla);
        }
        else{ 
            if(precio <= 99.99){
                alert("Precio demasiado bajo, debe ser mayor a 99.99");
                plantilla = '<li><p>Precio demasiado bajo, debe ser mayor a 99.99</p></li>'
                $('#contenedor').html(plantilla);
            }
        }
    });

    $( '#units' ).blur( function () {
        unidades = parseInt($('#units').val());
        if(unidades < 0){
            alert("Unidades debe ser un número mayor a 0");
            plantilla = '<li><p>Unidades debe ser un número mayor a 0</p></li>'
            $('#contenedor').html(plantilla);
        }
    });

    $( '#model' ).blur( function () {
        modelo = $('#model').val();
        if(modelo.length > 25){
            alert("Modelo demasiado largo, debe ser menor a 100 caracteres");
            plantilla = '<li><p>Modelo demasiado largo, debe ser menor a 100 caracteres</p></li>'
            $('#contenedor').html(plantilla);
        }
    });

    $( '#brand' ).blur( function () {
        marca = $('#brand').val();
    });

    $( '#details' ).blur( function () {
        detalles = $('#details').val();
        if(detalles > 25){
            alert("Detalles demasiado largo, debe ser menor a 25 caracteres");
            plantilla = '<li><p>Detalles demasiado largo, debe ser menor a 25 caracteres</p></li>'
            $('#contenedor').html(plantilla);
        }
    });

    $( '#image' ).blur( function () {
        imagen = $('#image').val();
        if(imagen == ''){
            $('#image').val = 'img/default.png';
        }
    });

    $('#product-form').submit(function (e) {
        nombre = $('#name').val();
        var mensajesError = [];
        var error = false;

        if(nombre == null || nombre == ''){
            mensajesError.push("Ingresar el nombre");
            error = true;
        }
        if(nombre > 100){
            mensajesError.push("Nombre demasiado largo, debe ser menor a 100 caracteres");
            error = true;
        }
        if(marca == null || marca == ''){
            mensajesError.push("Ingresar la marca");
        }
        if(modelo == null || modelo == ''){
            mensajesError.push("Ingresar el modelo");
            error = true;
        }
        if(modelo.length > 25){
            mensajesError.push("Modelo demasiado largo, debe ser menor a 25 caracteres")
            error = true;
        }
       
        if(detalles != null || detalles != ''){
            if(detalles.length > 250){
                mensajesError.push("Detalles demasiado largo, debe ser menor a 250 caracteres");
                error = true;
            }
        }
        if(unidades == null || unidades == ''){
            mensajesError.push("Ingresar unidades");
            error = true;
        }
        if(unidades < 0){
            mensajesError.push("Unidades debe ser un número mayor a 0");
            error = true;
        }
        if(imagen == null || imagen == ''){
            imagen = "img/default.png";
        }
        if(error == true){
            alert(mensajesError.join(', '));
        }
        else{
            nombre = $('#name').val();
            marca = $('#brand option:selected').val();
            modelo = $('#model').val();
            precio = $('#price').val();
            detalles = $('#details').val();
            unidades = $('#units').val();
            imagen = $('#image').val();
            const postData = {
                nombre: nombre,
                marca: marca,
                modelo: modelo,
                precio: precio,
                detalles: detalles,
                unidades: unidades,
                imagen: imagen,
                id: $('#productID').val()
            };

            let url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';

            $.post(url, postData, function (response) {
                fetchProducts();
                $('#product-form').trigger('reset');
                console.log(response);
            });
            e.preventDefault();
        }      
    });

    function fetchProducts(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function (response) {
                let productos = JSON.parse(response);
                let template = '';
                productos.forEach(producto => {
                    // SE COMPRUEBA QUE SE OBTIENE UN OBJETO POR ITERACIÓN
                    //console.log(producto);

                    // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                    template += `
                        <tr productId="${producto.id}">
                            <td>${producto.id}</td>
                            <td><a href="#" class="product-item">${producto.nombre}</a></td>
                            <td><ul>${producto.detalles}</ul></td>
                            <td>
                                <button class="product-delete btn btn-danger">
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    `
                });
                $('#products').html(template);
            }
        });
    }

    $(document).on('click', '.product-delete', function () {
        if(confirm('¿Estas seguro que deseas eliminar este registro?')) {
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            $.post('backend/product-delete.php', {id}, function (response) {
                fetchProducts();
                console.log(response);
            })
        }
    });

    $(document).on('click', '.product-item', function() {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');
        $.post('backend/product-single.php', {id}, function(response) {
            const product = JSON.parse(response);
            $('#productID').val(product.id);
            $('#name').val(product.nombre);
            $('#description').val(`{
                "precio": ${product.precio},
                "unidades": ${product.unidades},
                "modelo": "${product.modelo}",
                "marca": "${product.marca}",
                "detalles": "${product.detalles}",
                "imagen": "${product.imagen}"
            }`);
            edit = true;
        });
    });
});