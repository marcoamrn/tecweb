<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title> Practica 04 </title>
    </head>
    <body>
        <?php
            $arreglo["GIT4958"] = array(
                             "Auto" => array("Marca" => "Mercedes Benz",
                                             "Modelo" => 2022,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "Marco Londra",
                                                    "Ciudad" => "Puebla")
                        );
            $arreglo["NSI9958"] = array(
                             "Auto" => array("Marca" => "Nissan",
                                             "Modelo" => 2022,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "Lizbeth Hernandez",
                                                    "Ciudad" => "Libres")
                       );
            $arreglo["FKU5990"] = array(
                             "Auto" => array("Marca" => "Cadillac",
                                             "Modelo" => 2022,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "Yoghun Garza",
                                                    "Ciudad" => "Puebla")
                        );
            $arreglo["BSL5860"] = array(
                             "Auto" => array("Marca" => "Toyota",
                                             "Modelo" => 2019,
                                             "Tipo" => "hachback"),
                             "Propietario" => array("Nombre" => "Benito Martinez",
                                                    "Ciudad" => "Monterrey")
                        );
            $arreglo["BSK5896"] = array(
                             "Auto" => array("Marca" => "VolksWagen",
                                             "Modelo" => 2019,
                                             "Tipo" => "sedan"),
                             "Propietario" => array("Nombre" => "Khabib Nurmagomedov",
                                                    "Ciudad" => "Ciudad de Mexico")
                        );
            $arreglo["BKO5896"] = array(
                             "Auto" => array("Marca" => "BMW",
                                             "Modelo" => 2019,
                                             "Tipo" => "sedan"),
                             "Propietario" => array("Nombre" => "Stipe Miocic",
                                                    "Ciudad" => "Tijuana")
                       );
            $arreglo["HSN7906"] = array(
                             "Auto" => array("Marca" => "Toyota",
                                             "Modelo" => 2015,
                                             "Tipo" => "hachback"),
                             "Propietario" => array("Nombre" => "Francis Ngannou",
                                                    "Ciudad" => "Pachuca")
                        );
            $arreglo["FSI1539"] = array(
                             "Auto" => array("Marca" => "Mercedes Benz",
                                             "Modelo" => 2018,
                                             "Tipo" => "sedan"),
                             "Propietario" => array("Nombre" => "Nate Diaz",
                                                    "Ciudad" => "Puebla")
                        );
            $arreglo["SUF1639"] = array(
                             "Auto" => array("Marca" => "BMW",
                                             "Modelo" => 2022,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "TJ Dilashaw",
                                                    "Ciudad" => "Monterrey")
                        );
            $arreglo["NDI4858"] = array(
                             "Auto" => array("Marca" => "VolksWagen",
                                             "Modelo" => 2015,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "Cody Garbrandt",
                                                    "Ciudad" => "Tlatlauchi")
                        );
            $arreglo["SUM5828"] = array(
                             "Auto" => array("Marca" => "Nissan",
                                             "Modelo" => 2015,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "Henry Cejudo",
                                                    "Ciudad" => "Guadalajara")
                        );
            $arreglo["JDO5178"] = array(
                             "Auto" => array("Marca" => "Cadillac",
                                             "Modelo" => 2019,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "Justin Gaethje",
                                                    "Ciudad" => "Veracruz")
                       );
            $arreglo["SNC1854"] = array(
                             "Auto" => array("Marca" => "Toyota",
                                             "Modelo" => 2018,
                                             "Tipo" => "sedan"),
                             "Propietario" => array("Nombre" => "Brock Lesnar",
                                                    "Ciudad" => "Monterrey")
                        );
            $arreglo["DHN4528"] = array(
                             "Auto" => array("Marca" => "Audi",
                                             "Modelo" => 2020,
                                             "Tipo" => "camioneta"),
                             "Propietario" => array("Nombre" => "Valentina Shevshenko",
                                                    "Ciudad" => "Tlaxcala")
                        );
            $arreglo["DNO1452"] = array(
                             "Auto" => array("Marca" => "BMW",
                                             "Modelo" => 2021,
                                             "Tipo" => "hachback"),
                             "Propietario" => array("Nombre" => "Ronda Rousey",
                                                    "Ciudad" => "Veracruz")
                        );
            $matricula = $_POST['matricula'];
            if($matricula == "")
            {
                echo ('<pre>');
                print_r($arreglo);
                echo ('</pre>');
            }
            else
            {
                print_r($arreglo["$matricula"]);
            }
        ?>
    </body>
</html>