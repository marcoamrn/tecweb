<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title> Practica 03 </title>
    </head>
    <body>
        <?php
                echo "<h2>Ejercicio 1 - Variables válidas</h2>";
                echo "\$_myvar: La variable inicia con signo de pesos y su nombre con un guion bajo - Es valido<br>";
                echo "\$_7var: La variable inicia con signo de pesos y su nombre con un guion bajo - Es valido<br>";
                echo "\myvar: La variable NO inicia con signo de pesos - No es valido<br>";
                echo "\$myvar: La variable inicia con signo de pesos y su nombre con una letra - Es valido<br>";
                echo "\$var7: La variable inicia con signo de pesos y su nombre con una letra - Es valido<br>";
                echo "\$_element1: La variable inicia con signo de pesos y su nombre con guion bajo - Es valido<br>";
                echo "\$house*5: La variable inicia con signo de pesos y su nombre con una letra, el * no esta en el rango ASCII valido - No es valido<br>";

                echo "<h2>Ejercicio 2 - Valores</h2>";
                $a = "ManejadorSQL";
                $b = 'MySQL';
                $c = &$a;
                echo "<h3>Mostrar el contenido de cada variable</h3>";
                echo "\$a = $a <br>";
                echo "\$b = $b <br>";
                echo "\$c = $c <br>";
                //Agregar las asignaciones
                $a = "PHP server";
                $b = &$a;
                echo "<h3>Contenido de cada variable</h3>";
                echo "\$a = $a <br>";
                echo "\$b = $b <br>";
                echo "\$c = $c <br>";
                echo "<h3>Explicación</h3>";
                echo "<p>El valor de \$a se cambió a PHP server, despues \$b se cambió por referencia a la variable \$a cambiandolo a PHP server
                y \$c se actualiza a PHP server porque hace referencia a \$a</p>";

                echo "<h2>Ejercicio 3 - Variable</h2>";
                $a = "PHP5";
                echo "\$a = $a <br>";
                $z[] = &$a;
                echo "\$z = ";
                echo print_r($z);
                $b = "5a version de PHP";
                echo "<br>\$b = $b <br>";
                $c = str_repeat($b, 10);
                echo "\$c = $c <br>";
                //En el caso de c no se puede hacer una multiplicacion a un string, asi que la funcion repite la variable diez veces
                $a .= $b;
                echo "\$a = $a <br>";
                $b *= $c;
                echo "\$b = $b <br>";
                echo "Se está diciendo que a \$b se le multiplique el valor de \$c pero \$c es string <br>";
                $z[0] = "MySQL";
                echo "\$z[0] = $z[0] <br>";

                echo "<h2>Ejercicio 4 - \$GLOBALS</h2>";
                echo $GLOBALS['a'] . "<br>";
                echo $GLOBALS['b'] . "<br>";
                echo $GLOBALS['c'] . "<br>";
                echo $GLOBALS['z'] . "<br>";
                $z = print_r($z);
                echo "La variable \$z es un arreglo y con \$GLOBALS se convierte a un string";
                unset($a);
                unset($b);
                unset($c);

                echo "<h2>Ejercicio 5 - Valor</h2>";
                $a = "7 personas";
                $b = (integer) $a;
                $a = "9E3";
                $c = (double) $a;
                echo "\$a = $a <br>";
                echo "\$b = $b <br>";
                echo "\$c = $c <br>";
                unset($a);
                unset($b);
                unset($c);

                echo "<h2>Ejercicio 6 - Variables bool</h2>";
                $a =  "0";
                $b = "TRUE";
                $c = FALSE;
                $d = ($a or $b);
                $e = ($a AND $c);
                $f = ($a XOR $b);
                echo "\$a = ";
                var_dump($a);
                echo "<br> \$b = ";
                var_dump($b);
                echo "<br> \$c = ";
                var_dump($c);
                echo "<br> \$d = ";
                var_dump($d);
                echo "<br> \$e = ";
                var_dump($e);
                echo "<br> \$f = ";
                var_dump($f);
                unset($a);
                unset($b);
                unset($c);
                unset($d);
                unset($e);
                unset($f);

                echo "<h2>Ejercicio 7 - \$_SERVER</h2>";
                echo "<h3>Versión de Apache y PHP</h3>";
                echo $_SERVER['SERVER_SIGNATURE'];
                echo "<h3>Nombre del sistema operativo</h3>";
                echo $_SERVER['SERVER_NAME'];
                echo "<h3>Idioma del navegador</h3>";
                echo $_SERVER['HTTP_ACCEPT_LANGUAGE'];

                
        ?>
        <p>
            <a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
        </p>
    </body>
</html>