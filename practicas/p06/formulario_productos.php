<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 06</title>
    </head>
    <body>
        <form action="http://localhost/p06/Ejercicio%202/set_producto_v2.php" method="post">
            <ul>
                <li><label for="name">Nombre : </label><input type="text" name="name"></li>
                <li><label for="brand">Marca: </label><input type="text" name="brand"></li>
                <li><label for="model">Modelo: </label><input type="text" name="model"></li>
                <li><label for="price">Precio: </label><input type="number" name="price" step="any"></li>
                <li><label for="details">Detalles: </label><input type="text" name="details"></li>
                <li><label for="units">Unidades: </label><input type="number" name="units" step="any"></li>
                <li><label for="image">Imagen: </label><input type="text" name="image"></li>
            </ul>
            <input type="submit" value="Enviar">
        </form>
    </body>
</html>