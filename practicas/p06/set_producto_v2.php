<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title> Practica 06 </title>
    </head>
    <body>
        <?php
            @$link = new mysqli('localhost', 'root', 'Chabeleitor777', 'marketzone');

            if ($link->connect_errno) 
            {
                die('Falló la conexión: '.$link->connect_error.'<br/>');
                /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
            }

            $nombre = $_POST['name'];
            $marca = $_POST['brand'];
            $modelo = $_POST['model'];
            $precio = floatval($_POST['price']);
            $detalles = $_POST['details'];
            $unidades = intval($_POST['units']);
            $imagen = $_POST['image'];

            $vacio = true;
            $nombrestring = false;
            $marcastring = false;
            $modelostring = false;
            $preciofloat = false;
            $detallesstring = false;
            $unidadesint = false;
            $imagenstring = false;
            $tipo = false;

            if(empty($nombre) == false && empty($marca) == false && empty($modelo) == false && empty($precio) == false && empty($detalles) == false && empty($unidades) == false && empty($imagen) == false)
            {
                $vacio = false;
            }

            if(is_string($nombre) == true)
            {
                $nombrestring = true;
            }
            if(is_string($marca) == true)
            {
                $marcastring = true;
            }
            if(is_string($modelo) == true)
            {
                $modelostring = true;
            }
            if(is_float($precio) == true)
            {
                $preciofloat = true;
            }
            if(is_string($detalles) == true)
            {
                $detallesstring = true;
            }
            if(is_int($unidades) == true)
            {
                $unidadesint = true;
            }
            if(is_string($imagen) == true)
            {
                $imagenstring = true;
            }

            if($nombrestring == true && $marcastring == true && $modelostring == true && $preciofloat == true && $detallesstring == true && $unidadesint == true && $imagenstring == true)
            {
                $tipo = true;
            }

            if($vacio == false && $tipo == true)
            {
                echo "Nombre : $nombre<br>";
                echo "Marca : $marca<br>";
                echo "Modelo : $modelo<br>";
                echo "Precio : $precio<br>";
                echo "Detalles : $detalles<br>";
                echo "Unidades : $unidades<br>";
                echo "Imagen : $imagen<br>";
                $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', 0)";
                if ( $link->query($sql) ) 
                {
                    echo 'Producto insertado con ID: '.$link->insert_id;
                }
                else
                {
	                echo 'El Producto no pudo ser insertado =(';
                }
            }
            else
            {
                if($vacio == true)
                {
                    echo "No se pueden proporcionar datos vacíos<br>";
                }
                if($tipo == false)
                {
                    if(is_string($nombre) == false)
                    {
                        echo "El nombre debe ser una cadena de caracteres<br>";
                    }
                    if(is_string($marca) == false)
                    {
                        echo "La marca debe ser una cadena de caracteres<br>";
                    }
                    if(is_string($modelo) == false)
                    {
                        echo "El modelo debe ser una cadena de caracteres<br>";
                    }
                    if(is_float($precio) == false)
                    {
                        echo "El precio debe ser un número con decimales<br>";
                    }
                    if(is_string($detalles) == false)
                    {
                        echo "Los detalles debe ser una cadena de caracteres<br>";
                    }
                    if(is_int($unidades) == false)
                    {
                        echo "Las unidades debe ser un número entero<br>";
                    }
                    if(is_string($imagen) == false)
                    {
                        echo "La imagen debe ser una cadena de caracteres<br>";
                    }
                }
            }
            $link->close();
        ?>
    </body>
</html>