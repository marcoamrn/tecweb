// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
  };

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;

    // SE LISTAN TODOS LOS PRODUCTOS
}

$(document).ready(function () {
    let edit = false;
    $('#product-result').hide();
    fetchProducts();

    $('#search').keyup(function (e) {
        if ($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                type: 'GET',
                data: { search },
                success: function (response) {
                    console.log(response);
                    let products = JSON.parse(response);
                    let template = '';

                    products.forEach(product => {
                        template += `<li>
                            ${product.nombre}
                        </li>`
                    });

                    $('#container').html(template);
                    $('#product-result').show();
                }
            });
        }
    });

    $('#product-form').submit(function (e) {
        var productoJsonString = document.getElementById('description').value;
        // SE CONVIERTE EL JSON DE STRING A OBJETO
        var finalJSON = JSON.parse(productoJsonString);
        // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
        finalJSON['nombre'] = document.getElementById('name').value;
        // SE OBTIENE EL STRING DEL JSON FINAL
        productoJsonString = JSON.stringify(finalJSON,null,2);

        var pr = JSON.parse(productoJsonString);

        let nombre = pr['nombre'];
        let marca = pr['marca'];
        let modelo = pr['modelo'];
        let precio = parseFloat(pr['precio']);
        let detalles = pr['detalles'];
        let unidades = parseInt(pr['unidades']);
        let imagen = pr['imagen'];

        var mensajesError = [];
        var error = false;

        if(nombre == null || nombre == ''){
            mensajesError.push("Ingresar el nombre");
            error = true;
        }
        if(nombre > 100){
            mensajesError.push("Nombre demasiado largo, debe ser menor a 100 caracteres");
            error = true;
        }
        if(marca == null || marca == ''){
            mensajesError.push("Ingresar la marca");
        }
        if(modelo == null || modelo == ''){
            mensajesError.push("Ingresar el modelo");
            error = true;
        }
        if(modelo.length > 25){
            mensajesError.push("Modelo demasiado largo, debe ser menor a 25 caracteres")
            error = true;
        }
        if(precio == null || precio == ''){
            mensajesError.push("Ingresar el precio");
            error = true;
        }
        else{ 
            if(precio <= 99.99){
                mensajesError.push("Precio demasiado bajo, debe ser mayor a 99.99");
                error = true;
            }
        }
        if(detalles != null || detalles != ''){
            if(detalles.length > 250){
                mensajesError.push("Detalles demasiado largo, debe ser menor a 250 caracteres");
                error = true;
            }
        }
        if(unidades == null || unidades == ''){
            mensajesError.push("Ingresar unidades");
            error = true;
        }
        if(unidades < 0){
            mensajesError.push("Unidades debe ser un número mayor a 0");
            error = true;
        }
        if(imagen == null || imagen == ''){
            imagen.value = "img/default.png";
        }
        if(error == true){
            alert(mensajesError.join(', '));
        }
        else{
            const postData = {
                nombre: pr["nombre"],
                marca: pr["marca"],
                modelo: pr["modelo"],
                precio: pr["precio"],
                detalles: pr["detalles"],
                unidades: pr["unidades"],
                imagen: pr["imagen"],
                id: $('#productID').val()
            };

            let url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';

            $.post(url, postData, function (response) {
                fetchProducts();
                $('#product-form').trigger('reset');
                console.log(response);
            });
            e.preventDefault();
        }
    });

    function fetchProducts(){
        $.ajax({
            url: 'backend/product-list.php',
            type: 'GET',
            success: function (response) {
                let productos = JSON.parse(response);
                let template = '';
                productos.forEach(producto => {
                    // SE COMPRUEBA QUE SE OBTIENE UN OBJETO POR ITERACIÓN
                    //console.log(producto);

                    // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                    template += `
                        <tr productId="${producto.id}">
                            <td>${producto.id}</td>
                            <td><a href="#" class="product-item">${producto.nombre}</a></td>
                            <td><ul>${producto.detalles}</ul></td>
                            <td>
                                <button class="product-delete btn btn-danger">
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    `
                });
                $('#products').html(template);
            }
        });
    }

    $(document).on('click', '.product-delete', function () {
        if(confirm('¿Estas seguro que deseas eliminar este registro?')) {
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            $.post('backend/product-delete.php', {id}, function (response) {
                fetchProducts();
                console.log(response);
            })
        }
    });

    $(document).on('click', '.product-item', function() {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');
        $.post('backend/product-single.php', {id}, function(response) {
            const product = JSON.parse(response);
            $('#productID').val(product.id);
            $('#name').val(product.nombre);
            $('#description').val(`{
                "precio": ${product.precio},
                "unidades": ${product.unidades},
                "modelo": "${product.modelo}",
                "marca": "${product.marca}",
                "detalles": "${product.detalles}",
                "imagen": "${product.imagen}"
            }`);
            edit = true;
        });
    });
});