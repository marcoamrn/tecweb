<?php
use function PHPSTORM_META\type;

    include_once __DIR__.'/database.php';

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');
    if(!empty($producto)) {
        // SE TRANSFORMA EL STRING DEL JASON A OBJETO
        $jsonOBJ = json_decode($producto);
        /**
         * SUSTITUYE LA SIGUIENTE LÍNEA POR EL CÓDIGO QUE REALICE
         * LA INSERCIÓN A LA BASE DE DATOS. COMO RESPUESTA REGRESA
         * UN MENSAJE DE ÉXITO O DE ERROR, SEGÚN SEA EL CASO.
         */
        echo '[SERVIDOR] Nombre: '.$jsonOBJ->nombre;
        echo '[SERVIDOR] Modelo: '.$jsonOBJ->modelo;

        $nombre = $jsonOBJ->nombre;
        $marca = $jsonOBJ->marca;
        $modelo = $jsonOBJ->modelo;
        $precio = floatval($jsonOBJ->precio);
        $detalles = $jsonOBJ->detalles;
        $unidades = intval($jsonOBJ->unidades);
        $imagen = $jsonOBJ->imagen;

        $resultado = "SELECT * FROM productos WHERE nombre = '{$nombre}' AND eliminado = 0";
        $ncolumnas = $conexion->query($resultado);

        if( $ncolumnas->num_rows>0) {
                echo 'Ya existe un producto con este nombre';
        } else {
            $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', 0)";
            if ( $conexion->query($sql) ) 
            {
                echo 'Producto insertado con ID: '.$conexion->insert_id;
            }
            else
            {
	            echo 'El Producto no pudo ser insertado =(';
            }
        }
    }
?>