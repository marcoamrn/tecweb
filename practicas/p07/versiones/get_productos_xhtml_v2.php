<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title> Practica 07 </title>
        <link rel="stylesheet" href= "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity= "sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous" />
        <script>
            function show() {
                // se obtiene el id de la fila donde está el botón presinado
                var rowId = event.target.parentNode.parentNode.id;

                // se obtienen los datos de la fila en forma de arreglo
                var data = document.getElementById(rowId).querySelectorAll(".row-data");
                /**
                querySelectorAll() devuelve una lista de elementos (NodeList) que 
                coinciden con el grupo de selectores CSS indicados.
                (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

                En este caso se obtienen todos los datos de la fila con el id encontrado
                y que pertenecen a la clase "row-data".
                */

                var id = parseInt(data[0].innerHTML);
                var name = data[1].innerHTML;
                var brand = data[2].innerHTML;
                var model = data[3].innerHTML;
                var price = data[4].innerHTML;
                var units = parseInt(data[5].innerHTML);
                var details = data[6].innerHTML;
                var image = data[7].firstChild.getAttribute('src');

                alert("ID: " + id + "\nNombre: " + name + "\nMarca: " + brand + "\nModelo: " + model + "\nPrecio: " + price + "\nUnidades: " + units + "\nDetalles: " + details + "\nImagen: " + image);

                send2form(id, name, brand, model, price, units, details, image);
            }
        </script>
    </head>
    <body>
        <?php
            $data = array();

	        if(isset($_GET['tope']))
            {
		        $tope = $_GET['tope'];
            }
            else
            {
                die('Parámetro "tope" no detectado...');
            }

	        if (!empty($tope))
	        {
		        /** SE CREA EL OBJETO DE CONEXION */
		        @$link = new mysqli('localhost', 'root', 'Chabeleitor777', 'marketzone');
                /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */

		        /** comprobar la conexión */
		        if ($link->connect_errno) 
		        {
			        die('Falló la conexión: '.$link->connect_error.'<br/>');
			        //exit();
		        }

		        /** Crear una tabla que no devuelve un conjunto de resultados */
		        $result = "SELECT * FROM productos WHERE unidades <= $tope"; 
                    /** Se extraen las tuplas obtenidas de la consulta */
			    $query = mysqli_query($link,$result);
                $data = mysqli_fetch_array($query);
	        }
        ?>
        <div class="container">
            <table class="table mb-3">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Unidades</th>
                        <th scope="col">Detalles</th>
                        <th scope="col">Imagen</th>
                        <th scope="col">Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($query as $row){?>
                        <tr id="<?= $row['id'] ?>">
                            <td scope="row" class="row-data"><?= $row['id'] ?></th>
                            <td class="row-data"><?= $row['nombre']?></td>
                            <td class="row-data"><?= $row['marca']?></td>
                            <td class="row-data"><?= $row['modelo']?></td>
                            <td class="row-data"><?= $row['precio']?></td>
                            <td class="row-data"><?= $row['unidades']?></td>
                            <td class="row-data"><?= utf8_encode($row['detalles'])?></td>
                            <td class="row-data"><img src="<?= $row['imagen']?>" style="width:100%"/></td>
                            <td><input type="button" value="Editar" onclick="show()" /></td>
                        </tr>
                </tbody>
                <?php
                    }
                ?>
            </table>
        </div>
        <script>
            function send2form(id, name, brand, model, price, units, details, image) {
                var form = document.createElement("form");

                var idIn = document.createElement("input");
                idIn.type = 'number';
                idIn.name = 'id';
                idIn.value = id;
                form.appendChild(idIn);
                                
                var nombreIn = document.createElement("input");
                nombreIn.type = 'text';
                nombreIn.name = 'nombre';
                nombreIn.value = name;
                form.appendChild(nombreIn);

                var marcaIn = document.createElement("select");
                marcaIn.name = 'marca';
                marcaIn.value = brand;
                form.appendChild(marcaIn);

                var modeloIn = document.createElement("input");
                modeloIn.type = 'text';
                modeloIn.name = 'modelo';
                modeloIn.value = model;
                form.appendChild(modeloIn);

                var precioIn = document.createElement("input");
                modeloIn.type = 'text';
                precioIn.name = 'precio';
                precioIn.value = price;
                form.appendChild(precioIn);

                var unidadesIn = document.createElement("input");
                unidadesIn.type = 'number';
                unidadesIn.name = 'unidades';
                unidadesIn.value = units;
                form.appendChild(unidadesIn);

                var detallesIn = document.createElement("input");
                detallesIn.type = 'text';
                detallesIn.name = 'detalles';
                detallesIn.value = details;
                form.appendChild(detallesIn);

                var imagenIn = document.createElement("input");
                imagenIn.type = 'text';
                imagenIn.name = 'imagen';
                imagenIn.value = image;
                form.appendChild(imagenIn);

                console.log(form);

                form.method = 'POST';
                form.action = 'http://localhost/p07/versiones/formulario_productos_v3.php';  

                document.body.appendChild(form);
                form.submit();
            }
        </script>
    </body>
</html>