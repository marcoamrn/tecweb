<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 07</title>
    </head>
    <body>
        <form action="http://localhost/p07/validaci%c3%b3n/set_producto_v2.php" method="post">
            <ul>
                <li><label for="name">Nombre : </label><input type="text" name="name" id="name"></li>
                <li><label for="brand">Marca: </label><select name="brand" id="brand">
                    <option value="Pull&Bear">Pull&Bear</option>
                    <option value="Levis">Levis</option>
                    <option value="Aeropostale">Aeropostale</option>
                    <option value="Nike">Nike</option>
                    <option value="Adidas">Adidas</option>
                </select></li>
                <li><label for="model">Modelo: </label><input type="text" name="model" id="model"></li>
                <li><label for="price">Precio: </label><input type="number" name="price" step="any" id="price"></li>
                <li><label for="details">Detalles: </label><input type="text" name="details" id="details"></li>
                <li><label for="units">Unidades: </label><input type="number" name="units" step="any" id="units"></li>
                <li><label for="image">Imagen: </label><input type="text" name="image" id="image"></li>
            </ul>
            <input type="submit" value="Enviar" id="send" onClick="return enviarFormulario();">
        </form>
        <div id="e"></div>
        <script type="text/javascript">
            var nombre = document.getElementById("name");
            var marca = document.getElementById("brand");
            var modelo = document.getElementById("model");
            var precio = document.getElementById("price");
            var detalles = document.getElementById("details");
            var unidades = document.getElementById("units");
            var imagen = document.getElementById("image");
            var e = document.getElementById("e");

            function enviarFormulario(){
                var mensajesError = [];
                var error = false;

                if(nombre.value == null || nombre.value == ''){
                    mensajesError.push("Ingresar el nombre");
                    error = true;
                }
                if(nombre.length > 100){
                    mensajesError.push("Nombre demasiado largo, debe ser menor a 100 caracteres");
                    error = true;
                }
                if(marca.value == null || marca.value == ''){
                    mensajesError.push("Ingresar la marca");
                }
                if(modelo.value == null || modelo.value == ''){
                    mensajesError.push("Ingresar el modelo");
                    error = true;
                }
                if(modelo.lenth > 25){
                    mensajesError.push("Modelo demasiado largo, debe ser menor a 25 caracteres")
                    error = true;
                }
                if(precio.value == null || precio.value == ''){
                    mensajesError.push("Ingresar el precio");
                    error = true;
                }
                else{ 
                    if(precio.value <= 99.99){
                        mensajesError.push("Precio demasiado bajo, debe ser mayor a 99.99");
                        error = true;
                    }
                }
                if(detalles.value != null || detalles.value != ''){
                    if(detalles.length > 250){
                        mensajesError.push("Detalles demasiado largo, debe ser menor a 250 caracteres");
                        error = true;
                    }
                }
                if(unidades.value == null || unidades.value == ''){
                    mensajesError.push("Ingresar unidades");
                    error = true;
                }
                if(unidades.value < 0){
                    mensajesError.push("Unidades debe ser un número mayor a 0");
                    error = true;
                }
                if(imagen.value == null || imagen.value == ''){
                    imagen.value = "img/default.png";
                }
                if(error == true){
                    e.innerHTML = mensajesError.join(', ');
                    return false;
                }
                else{
                    return true;
                }
            }
        </script>
    </body>
</html>